function filter(elements, cb) {
    let res = [];
    for(let index = 0; index < elements.length; index++) {
        let calculate = cb(elements[index]);
        if(calculate === true){
            res.push(elements[index]);
        }
    }
    return res;
}

export default filter;