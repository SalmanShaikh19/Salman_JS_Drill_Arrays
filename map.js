function map(elements, cb) {
    const res = [];
    for(let index = 0; index < elements.length; index++){
        const func = cb(elements[index], index);
        res.push(func);
    }
    return res;
}

export default map;