import find from "../find.js"

const items = [1, 2, 3, 4, 5, 5];

function cb (num) {
    return num % 2 === 0; 
}

const result = find(items,cb);
console.log(result);