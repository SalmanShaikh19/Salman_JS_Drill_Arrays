function each(elements, cb) {
    for(let index = 0; index < elements.length; index++) {
        const res = cb(elements[index],index);
    }
}

export default each;