function find(elements, cb) {

    for(let index = 0; index < elements.length; index++) {
        let element = elements[index];
        let boolean = cb(element);
        if(boolean === true){
            return element;
        }
    }
}

export default find;