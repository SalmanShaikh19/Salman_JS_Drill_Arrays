import each from "../each.js"

const items = [1, 2, 3, 4, 5, 5];

function cb (element,index) {
    element = element*10;
    console.log("Modified element is " + element);
}

each(items,cb);
