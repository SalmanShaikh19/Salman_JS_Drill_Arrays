import filter from "../filter.js";

const items = [1, 2, 3, 4, 5, 5];

function cb (num) {
     return num % 2 === 0; 
}

const result = filter(items,cb);
console.log(result);
