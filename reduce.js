function reduce(elements, cb, startingValue = elements[0]) {

    let first = startingValue;
    for(let index = 1; index < elements.length; index++) {
        let calculate = cb(first,elements[index]);
        first = calculate;
    }
    return first;
}

export default reduce;