function flatten(elements) {
    const res = [];
    for(let index = 0; index < elements.length; index++) {
        let element = elements[index];
        if(Array.isArray(element)) {
            res.push(...flatten(element));
        }else{
            res.push(element);
        }
    }
    return res;
    
  }

export default flatten;